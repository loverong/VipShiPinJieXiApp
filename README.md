# Vip视频解析App

#### 介绍
Vip视频解析App：支持多种线路解析：腾讯，爱奇艺，土豆，芒果，哔哩哔哩，ACFun，乐视，搜狐，新浪，优酷等20多种Vip及付费视频

#### 软件架构
Vue.js + UNI组件

#### [点击这里下载App](https://gitee.com/itkenor/VipShiPinJieXiApp/attach_files/242570/download)

#### 安装教程

使用HBuild进行打包

#### TODO
- 可以不依赖浏览器地打开第三方网站并播放视频
- 收集更多的播放线路
- 去除第三方线路的播放广告窗口

#### 版权声明
- 本项目只适用于学习交流，切勿用于商业盈利，否则一切后果本人概不负责
- 访问者可将本网站提供的内容或服务用于个人学习、研究或欣赏，以及其他非商业性或非盈利性用途，但同时应遵守著作权法及其他相关法律的规定
- 本项目所使用到的`图片及第三方解析接口均来自网络`,如有侵权，请联系：`2295477997@qq.com` 进行删除

#### 使用说明

- 复制视频的播放地址
![输入图片说明](https://images.gitee.com/uploads/images/2019/0610/153540_79013d73_1905039.jpeg "微信图片_20190610153346.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0610/153557_60d99500_1905039.png "微信图片_20190610153359.png")
- 打开App，将复制好的链接粘贴到输入框内，点击播放即可（如果无法播放，请切换播放地址== :collision: ）
![输入图片说明](https://images.gitee.com/uploads/images/2019/0610/153608_891216a1_1905039.jpeg "微信图片_20190610153404.jpg")
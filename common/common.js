// 友情链接
const websiteUrl = '';  
// 播放链接
const pageUrl = '';
// html 数据
const htmlData = '';
// 当前的解析地址
const analysis = '';
// 解析列表
const urlList = [
	{name: "线路1(含第三方广告)",src: "http://jiexi.071811.cc/jx2.php?url="},
	{name: "线路2(含第三方广告)",src: "http://app.baiyug.cn:2019/vip/?url="},
	{name: "线路3(含第三方广告)",src: "http://tv.beipy.com/jx/?url="},
	{name: "线路4(含第三方广告)",src: "http://api.nepian.com/ckparse/?url="},
	{name: "线路5(含第三方广告)",src: "http://j.zz22x.com/jx/?url="},
	{name: "线路6(含第三方广告)",src: "http://api.wlzhan.com/sudu/?url="},
	{name: "线路7(含第三方广告)",src: "http://jqaaa.com/jq3/?url="},
	{name: "线路8(含第三方广告)",src: "http://aikan-tv.com/?url="}
];
// 友情链接列表
const imgList = [
	{src:'/static/img/1905logo.png',url:'https://vip.1905.com/'},
	{src:'/static/img/acfun.png',url:'https://www.acfun.cn/'},
	{src:'/static/img/baofeng.png',url:'http://www.baofeng.com/'},
	{src:'/static/img/bilibili.png',url:'https://www.bilibili.com/'},
	{src:'/static/img/cntvlogo.png',url:'http://tv.cctv.com/'},
	{src:'/static/img/fengxing.png',url:'http://www.fun.tv/'},
	{src:'/static/img/hunantvlogo.png',url:'https://www.mgtv.com/'},
	{src:'/static/img/iqiyilogo.png',url:'http://www.iqiyi.com/'},
	{src:'/static/img/kankan.png',url:'http://www.kankan.com/'},
	{src:'/static/img/letvlogo.png',url:'http://www.le.com/'},
	{src:'/static/img/pptv.png',url:'http://www.pptv.com/'},
	{src:'/static/img/qqlogo.png',url:'https://v.qq.com/'},
	{src:'/static/img/sinalogo.png',url:'http://video.sina.com.cn/'},
	{src:'/static/img/sohulogo.png',url:'https://film.sohu.com/'},
	{src:'/static/img/tudoulogo.png',url:'https://www.tudou.com/'},
	{src:'/static/img/wasulogo.png',url:'https://www.wasu.cn/'},
	{src:'/static/img/yinyuetailogo.png',url:'http://www.yinyuetai.com/'},
	{src:'/static/img/youkulogo.png',url:'https://www.youku.com/'}
]
export default {  
    websiteUrl,
	pageUrl,
	htmlData,
	analysis,
	urlList,
	imgList
} 